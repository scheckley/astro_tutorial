---
title: "My first blog post"
pubDate: 2023-08-27
description: "first blog post"
author: "me"
image:
    url: 'https://docs.astro.build/assets/full-logo-light.png'
    alt: 'The full Astro logo.'
tags: ['astro','blog','learning']
---

# My First Blog Post

Published on: 2023-08-27

Welcome to my _new blog_ I'm using to learn Astro!


